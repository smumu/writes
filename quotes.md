> Alors que tu disparais dans la foule, les secondes se rallongent. Mais le temps ne s'arrête pas. Alors, dans une éternité, tu sortiras de la foule et les heures deviendront des minutes, encore.

2019-12-21
[WTFPL license](http://www.wtfpl.net/)
