Les ailes étaient déployées, le bec face au vent frais et calme. Calme, majestueux, déterminé. La blancheur de ses plumes n'étaient pas même entachées par les extrémités noires de ses ailes. Il volait bien, il volait vite, il volait parfaitement. Que pourrait-il arriver qui pourrait faire faillir cette droiture et cette puissance ?
Et pourtant, il atterrit. Brusquement, tant bien que mal, sans autre issue que la douleur extrême d'un arrêt forcé et non prévu. Peut-être aurait-il pu continuer son trajet, toujours plus vite et plus loin. Mais non, alors que rien ne semblait pouvoir l'arrêter, il a plongé vers le sol, comme une flèche, sans même l'avoir prévu lui-même.
Son vol était le plus beau bien qu'éphémère, et il n'oubliera jamais la sensation qu'il put éprouver lors de ce court instant.

Atterrissage - Samuel DENIS, 12/03/2019
Licence Creative Commons CC-BY-NC-SA (https://creativecommons.org/licenses/by-nc-sa/4.0/).
